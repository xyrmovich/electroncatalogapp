const electron = require('electron');
const dialog = electron.dialog;
const url = require('url');
const path = require('path');
const db = require("./dev/build/Release/database");

const {app, BrowserWindow, Menu, ipcMain} = electron;

var mainWindow;
var addWindow;

process.env.NODE_ENV == 'production'

//Listen  for app to be ready
app.on('ready', ()=>{
    //Create new window
    mainWindow = new BrowserWindow({
        title: "AnimeMaid",
        webPreferences:{
            nodeIntegration:true
        }
    });
    //Load html file into window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    mainWindow.on('closed', ()=>{
        app.quit();
    });

    //set template menu
    const menuWindow = Menu.buildFromTemplate(menuWindowTemlete);
    Menu.setApplicationMenu(menuWindow);
});

async function createAddWindow(){
    //Create new window
    addWindow = new BrowserWindow({
        width: 400,
        height: 200,
        title: 'Input',
        webPreferences:{
            nodeIntegration:true
        }
    });

    addWindow.removeMenu();
    //Load html file into window
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    //Garbage collection
    addWindow.on('closed', ()=>{
        addWindow = null;
    });
};

function opendatabase(){
    console.log(db.query("open", "./dev/db.xyrm"));
    let data = db.query("select_all", "items");
    console.log(data);
    mainWindow.webContents.send('getdata:db', data);
}

ipcMain.on("open:db", (e, path)=>{
    db.query("open", path);
    let object = db.query("select_all", "items");
    mainWindow.webContents.send("load:db", object);
})

ipcMain.on('text:add', (e, value)=>{
    mainWindow.webContents.send('text:add', value);
    addWindow.close();
});

ipcMain.on('add:row', (e, obj, id)=>{
   let result = db.query('insert', 'items', obj);
   if(result != 0)
        obj.id = result;
   console.log(obj);
   if(result)
        mainWindow.webContents.send('succsess_add:row', obj);
});

ipcMain.on('delete:cell', (e, obj) =>{
    let result = db.query('update', 'items', [obj.target, '', obj.column, obj.condition]);
    if(result)
        mainWindow.webContents.send('succsess_delete:cell', obj);
});

ipcMain.on('delete:row', (e, obj)=>{
    let result = db.query('delete', 'items', ['ID', obj.id]);
    if(result)
        mainWindow.webContents.send('succsess_delete:row', obj);
});

ipcMain.on('edit:cell', (e, obj)=>{
    createAddWindow();
    addWindow.webContents.on('did-finish-load', ()=>{
        addWindow.webContents.send('getdata:edit', obj);
    });
});

ipcMain.on('edit_data:cell', (e, obj)=>{
    let result = db.query('update', 'items', [obj.target, obj.newcond, 'ID', obj.id]);
    if(result)
        mainWindow.webContents.send('edit_data:cell', obj);
    addWindow.close();
});

ipcMain.on('search:db', (e, keys)=>{
    createAddWindow();
    addWindow.webContents.on('did-finish-load', ()=>{
        addWindow.webContents.send('getdata:search', keys);
    });
});

ipcMain.on('search_data:db', (e, keys)=>{
    let arr = new Array;
    keys.array.forEach(column =>{
        arr.push(...db.query('select', 'items', column, keys.newcond));
    });
    mainWindow.webContents.send("search_data:db", arr);
    addWindow.close();
});

const chooseFolder = ()=>{
    dialog.showOpenDialog(mainWindow,{
        properties: ['openDirectory'],
    }).then(result =>{
        if(result.canceled == false){
            db.query('open', result.filePaths[0]+'\\newtable.xyrm');
            db.query('create_table', 'items');
            db.query('insert', 'items', {
                name: "",
                quantity: "",
                price:""
            });
            let obj = db.query('select_all', 'items');
            mainWindow.webContents.send('load:db', obj);
        }
    });
};

//Create menu template

const menuWindowTemlete = [
    {
        label: 'File',
        submenu : [
            {
                label : 'Crate database',
                click(){
                    chooseFolder();
                }
            },
            {
                label : 'Quit',
                accelerator : (process.platform == 'darwin') ? 'Command+Q' : 'Ctrl+Q',
                click(){
                    app.quit();
                }
            }
        ]
    }
];

if(process.platform == 'darwin'){
    menuWindowTemlete.unshift({});
}

/*if(process.env.NODE_ENV !== 'production'){
    menuWindowTemlete.push({
        label : 'Dev Tools',
        submenu: [
            {
                label: 'Toogle Dev Tools',
                accelerator : (process.platform == 'darwin') ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'Reload'
            }
        ]
    });
}*/

const electron = require('electron');
const {ipcRenderer} = electron;

const input_item = document.getElementById("record");
const button = document.getElementById("add_btn");

var obj;
var action;

button.addEventListener("click", (event)=>{
    console.log(obj);
    event.preventDefault();
    let text = input_item.value;
    obj.newcond = text;
    if(action == 'edit')
        ipcRenderer.send('edit_data:cell', obj);
    if(action == 'search')
        ipcRenderer.send('search_data:db', obj);
});

ipcRenderer.on('getdata:edit', (e, data) =>{
    console.log(data);
    obj = data;
    action = 'edit';
    input_item.value = data.prevcond;
});

ipcRenderer.on('getdata:search', (e, keys)=>{
    obj = new Object;
    obj.array = keys;
    action = 'search';
});

ipcRenderer.on('text:add', (e, data)=>{
    console.log(data);
});


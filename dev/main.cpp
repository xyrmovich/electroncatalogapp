#include <iostream>
#include "sql.hpp"
#include "callback.hpp"

int main(){
    Sql db;
    std::string dbname = "database.db";
    if(db.open(dbname.c_str())){
        std::cout << "Open successfully"<<std::endl;
    }
    else{
        std::cout << "Error, database doesnt open" << std::endl;
    }

    /*if(db.createTable("INFORMATION")){
        std::cout << "Adding table successfully" << std::endl;
    }
    else{
        std::cout << "Error" << std::endl;
    }

    if(db.createColumn("INFORMATION", "AGE", "INTEGER"))
        std::cout << "Column created" << std::endl;
    else{
        std::cout << "Column not created" << std::endl;
    }

    std::vector<std::string> value = {"'18'"};
    if(db.insertColumn("INFORMATION", "AGE",  value))
        std::cout << "Inserting done" << std::endl;
    else
        std::cout << "Insert error" << std::endl;
    
    std::vector<std::string> column_info = {"AGE", "18", "AGE", "IS NULL"};

    if(db.updateColumn("INFORMATION", column_info))
        std::cout << "Updated" << std::endl;
    else
        std::cout << "Not updated" << std::endl;
    */
    CallBack callback = db.getTable("INFORMATION");
    callback.show();
    return 0;
}
#pragma once
#include <iostream>
#include <vector>
#include "sqlite3.h"
#include "callback.hpp"

class Sql{
private:
    sqlite3 *database;
public:
    ~Sql();
    bool open(std::string);
    bool createTable(std::string);
    CallBack getTable(std::string);
    CallBack getExactTable(std::string table_name, std::string column_name, std::string column_value);
    bool createColumn(std::string, std::string, std::string);
    bool updateRow(std::string, std::vector<std::string>);
    int insertRow(std::string, std::vector<std::string>, std::vector<std::string>);
    bool deleteRow(std::string, std::vector<std::string>);
    friend int lastid(void*, int, char**, char**);
};

int lastid(void*, int, char**, char**);




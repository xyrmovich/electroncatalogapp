#include <iostream>
#include <vector>
#include "callback.hpp"

CallBack::CallBack(const CallBack& callback){
    this->argc = callback.argc;
    this->argv = callback.argv;
    this->column_name = callback.column_name;
}

CallBack CallBack::operator=(const CallBack& callback){
    this->argc = callback.argc;
    this->argv = callback.argv;
    this->column_name = callback.column_name;

    return *this;
}

int table_callback(void* data, int argc, char** argv, char** column_name){
    static_cast<CallBack*>(data)->argc = argc;
    for(size_t counter = 0; counter < argc ; ++counter){
       static_cast<CallBack*>(data)->argv.push_back(std::string(argv[counter]));
       static_cast<CallBack*>(data)->column_name.push_back(std::string(column_name[counter]));
    }
    return 0;
}

int CallBack::getArgc(){
    return this->argc;
}

std::vector<std::string> CallBack::getArgv(){
    return this->argv;
}

std::vector<std::string> CallBack::getColumnName(){
    return this->column_name;
}

void CallBack::show(){
    std::cout << argc << std::endl;
    for(size_t counter = 0; counter < this->argv.size(); ++counter){
        std::cout << this->column_name[counter] << " = " << this->argv[counter] << std::endl;
    }
}
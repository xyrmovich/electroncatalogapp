#include <node.h>
#include <v8.h>
#include "sql.hpp"
#include "callback.hpp"

Sql db;

void database(const v8::FunctionCallbackInfo<v8::Value>& args){
    v8::Isolate* isolate = args.GetIsolate();
    v8::String::Utf8Value js_str(isolate, args[0]);
    std::string action(*js_str);
    v8::Local<v8::Context> context = v8::Context::New(isolate);
    v8::Context::Scope context_scope(context);
    if(action == "open"){
        v8::String::Utf8Value js_db_location(isolate, args[1]);
        std::string file_location(*js_db_location);
        v8::Local<v8::Boolean> result = v8::Boolean::New(isolate, (db.open(file_location)));
        args.GetReturnValue().Set(result);
    }
    if(action == "create_table"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        v8::Local<v8::Boolean> result = v8::Boolean::New(isolate, (db.createTable(table_name)));
        args.GetReturnValue().Set(result);
    }
    if(action == "select_all"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        CallBack callback = db.getTable(table_name);
        v8::Local<v8::Array> ret_arr = v8::Array::New(isolate);
        std::vector<std::string> argv = callback.getArgv();
        std::vector<std::string> column_name = callback.getColumnName();
        int argc = callback.getArgc();
        for(size_t counter = 0, index = 0; counter < argv.size(); counter+=argc, ++index){
            v8::Local<v8::Object> obj = v8::Object::New(isolate);
            for(size_t i = 0; i<argc; ++i){
                v8::MaybeLocal<v8::String> str_column = v8::String::NewFromUtf8(isolate, (column_name[counter+i]).c_str());
                v8::MaybeLocal<v8::String> str_value = v8::String::NewFromUtf8(isolate, (argv[counter+i]).c_str());
                obj->Set(context ,str_column.ToLocalChecked(), str_value.ToLocalChecked());
            }
            ret_arr->Set(context, index, obj);
        }
        args.GetReturnValue().Set(ret_arr);
    }
    if(action == "select"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        v8::String::Utf8Value js_column_condition(isolate, args[2]);
        std::string column_condition(*js_column_condition);
        v8::String::Utf8Value js_value(isolate, args[3]);
        std::string value(*js_value);
        CallBack callback = db.getExactTable(table_name, column_condition, value);
        v8::Local<v8::Array> ret_arr = v8::Array::New(isolate);
        std::vector<std::string> argv = callback.getArgv();
        std::vector<std::string> column_name = callback.getColumnName();
        int argc = callback.getArgc();
        for(size_t counter = 0, index = 0; counter < argv.size(); counter+=argc, ++index){
            v8::Local<v8::Object> obj = v8::Object::New(isolate);
            for(size_t i = 0; i<argc; ++i){
                v8::MaybeLocal<v8::String> str_column = v8::String::NewFromUtf8(isolate, (column_name[counter+i]).c_str());
                v8::MaybeLocal<v8::String> str_value = v8::String::NewFromUtf8(isolate, (argv[counter+i]).c_str());
                obj->Set(context, str_column.ToLocalChecked(), str_value.ToLocalChecked());
            }
            ret_arr->Set(context, index, obj);
        }
        args.GetReturnValue().Set(ret_arr);
    }
    if(action == "insert"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        v8::Local<v8::Object> obj = v8::Local<v8::Object>::Cast(args[2]);
        std::vector<std::string> column_name = {"name", "quantity", "price"};
        std::vector<std::string> value;
        for(size_t counter = 0; counter < column_name.size(); ++counter){
            v8::MaybeLocal<v8::String> buff_str = v8::String::NewFromUtf8(isolate, (column_name[counter]).c_str());
            v8::String::Utf8Value str(isolate, obj->Get(context, buff_str.ToLocalChecked()).ToLocalChecked());
            std::cout << std::string(*str) << std::endl;
            value.push_back(std::string(*str));
        }
        v8::Local<v8::Number> result = v8::Number::New(isolate, (db.insertRow(table_name, column_name, value)));
        args.GetReturnValue().Set(result);
    }
    if(action == "update"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        v8::Local<v8::Array> arr = v8::Local<v8::Array>::Cast(args[2]);
        std::vector<std::string> inserting_line;
        for(size_t counter = 0; counter < 4; ++counter){
            v8::String::Utf8Value str(isolate, arr->Get(context, counter).ToLocalChecked());
            inserting_line.push_back(std::string(*str));
        }
        v8::Local<v8::Boolean> result = v8::Boolean::New(isolate, (db.updateRow(table_name, inserting_line)));
        args.GetReturnValue().Set(result);
    }
    if(action == "delete"){
        v8::String::Utf8Value js_table_name(isolate, args[1]);
        std::string table_name(*js_table_name);
        v8::Local<v8::Array> arr = v8::Local<v8::Array>::Cast(args[2]);
        std::vector<std::string> condition;
        for(size_t counter = 0; counter < 4; ++counter){
            v8::String::Utf8Value str(isolate, arr->Get(context, counter).ToLocalChecked());
            condition.push_back(std::string(*str));
        }
        v8::Local<v8::Boolean> result = v8::Boolean::New(isolate, (db.deleteRow(table_name, condition)));
        args.GetReturnValue().Set(result);
    }
}

void Init(v8::Local<v8::Object> exports){
    NODE_SET_METHOD(exports, "query", database);
}

NODE_MODULE(addon, Init);

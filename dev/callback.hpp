#pragma once
#include <iostream>
#include <vector>

class CallBack{
private:
    int argc;
    std::vector<std::string> argv;
    std::vector<std::string> column_name;
public:
    CallBack(int argc = 0, std::vector<std::string> argv = std::vector<std::string>(), 
        std::vector<std::string> column_name = std::vector<std::string>()) :
        argc(argc), argv(argv), column_name(column_name){};
    CallBack(const CallBack&);
    CallBack operator=(const CallBack&);
    void show();
    int getArgc();
    std::vector<std::string> getArgv();
    std::vector<std::string> getColumnName();
    friend int table_callback(void*, int, char**, char**);
};

int table_callback(void*, int, char**, char**);
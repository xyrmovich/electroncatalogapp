#include <iostream>
#include "sqlite3.h"
#include <vector>
#include <sstream>
#include "sql.hpp"
#include "callback.hpp"

Sql::~Sql(){
    sqlite3_close(this->database);
}

bool Sql::open(std::string file_name){
    int exec = sqlite3_open(file_name.c_str(), &(this->database));
    return (exec) ? false : true;
}

bool Sql::createTable(std::string table_name){
    std::string statment;
    char* errmsg = nullptr;
    statment = "CREATE TABLE " + table_name + " ("+
       +"ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name varchar(100), quantity varchar(20), price varchar(20));";
    int exec = sqlite3_exec(this->database, statment.c_str(), NULL, 0, &errmsg);
    if(errmsg)
        std::cerr << errmsg << std::endl;
    return (exec) ? false : true;
}


CallBack Sql::getTable(std::string table_name){
    std::string statment = "SELECT * from " + table_name + ";";
    char* errmsg = nullptr;
    CallBack callback;
    int exec = sqlite3_exec(this->database, statment.c_str(), table_callback, static_cast<void*>(&callback), &errmsg);
    if(errmsg)
        std::cerr << errmsg << std::endl;
    return (exec) ? CallBack() : callback;
}

CallBack Sql::getExactTable(std::string table_name, std::string column_name, std::string column_value){
    std::string statment = "SELECT * from " + table_name + " where "+column_name+" = '"+column_value + "';";
    char* errmsg = nullptr;
    CallBack callback;
    int exec = sqlite3_exec(this->database, statment.c_str(), table_callback, static_cast<void*>(&callback), &errmsg);
    if(errmsg)
        std::cerr << errmsg << std::endl;
    return (exec) ? CallBack() : callback;
}

bool Sql::createColumn(std::string table_name, std::string column_name, std::string column_type){
    std::string statment = "ALTER TABLE " + table_name + " add COLUMN " + column_name + " " + column_type + ";";
    char* errmsg = nullptr;
    int exec = sqlite3_exec(this->database, statment.c_str(), NULL, 0, &errmsg);
    if(errmsg)
        std::cerr << errmsg << std::endl;
    return (exec) ? false : true;
}

bool Sql::updateRow(std::string table_name, std::vector<std::string> inserting_line){
    std::string statment = "UPDATE " + table_name + " "
        "SET " + inserting_line[0] +  " = '" + inserting_line[1] + "' "
        "WHERE " + inserting_line[2] + " = '" + inserting_line[3] + "';";
    char* errmsg = nullptr;
    int exec = sqlite3_exec(this->database, statment.c_str(), NULL, 0, &errmsg);
    if(errmsg)
        std::cerr << errmsg << std::endl;

    return (exec) ? false : true; 
}

int Sql::insertRow(std::string table_name, std::vector<std::string> column_name, std::vector<std::string> value){
    std::string statment = "INSERT INTO " + table_name + " (";
    for(size_t counter = 0; counter < column_name.size(); ++counter){
        statment += column_name[counter] + (((counter+1) < value.size()) ? ", " : ") values (");
    }
    for(size_t counter = 0; counter < value.size(); ++counter){
        statment += "'" + value[counter] + (((counter+1) < value.size()) ? "', " : "');");
    }
    statment += " select last_insert_rowid();";
    int id = 0;
    char* errmsg = nullptr;
    int exec = sqlite3_exec(this->database, statment.c_str(), lastid, static_cast<void*>(&id), &errmsg);
    std::cout << id << std::endl;
    if(exec)
        std::cerr<< std::endl << errmsg << std::endl;
    return (exec) ? 0 : id;
}

bool Sql::deleteRow(std::string table_name, std::vector<std::string> condition){
    std::string statment = "delete from " + table_name + " where " + condition[0] + " = '" + condition[1] + "';";
    char* errmsg = nullptr;
    int exec = sqlite3_exec(this->database, statment.c_str(), NULL, 0, &errmsg);
    if(exec)
        std::cerr<< std::endl << errmsg << std::endl;

    return (exec) ? false : true;
}

int lastid(void* data, int argc, char** argv, char** column_name){
     std::stringstream ss;
     ss << std::string(argv[0]);
     ss >> *(static_cast<int*>(data));
     return 0;
}

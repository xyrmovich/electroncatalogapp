const electron = require('electron');
const {ipcRenderer} = electron;

const dropbox = document.getElementById('dropbox');
const container = document.getElementsByClassName('container');
const content = document.getElementsByClassName('content');
const input = document.getElementById('input');
const image = document.getElementById('image');
const buttonbox = document.getElementById('buttonbox');
const table = document.createElement('table');

var objectConditionHolder;
var lastId;

const dragenter = (e) =>{
    e.stopPropagation();
    e.preventDefault();
}

const dragover = (e) =>{
    e.stopPropagation();
    e.preventDefault();
}

const drop = (e) =>{
    e.preventDefault();
    const dt = e.dataTransfer;
    const files = dt.files;
    ipcRenderer.send("open:db", files[0].path);
    dropbox.style.display = 'none';
    image.style.display = 'none';
}

const boxclick = (e) =>{
    e.preventDefault();
    input.click();
}

function inputchange(e){
    e.preventDefault();
    ipcRenderer.send("open:db", this.files[0].path);
    dropbox.style.display = 'none';
    image.style.display = 'none';
}

const hideinput = () =>{
    dropbox.style.display = 'none';
    image.style.display = 'none';
}

dropbox.addEventListener('dragenter', dragenter, false);
dropbox.addEventListener('dragover', dragover, false);
dropbox.addEventListener('drop', drop, false);
dropbox.addEventListener('click', boxclick, false);
input.addEventListener('change', inputchange, false);
document.getElementById('add_btn').addEventListener('click', (e)=>{
    e.preventDefault();
    let tr = document.getElementsByTagName('tr');
    let keys = tr[0].childNodes;
    let obj = new Object;
    for(let i = 0; i < keys.length; ++i){
        obj[keys[i].childNodes[0].data] = "";
    }
    ipcRenderer.send('add:row', obj);
}, false);

document.getElementById('delete_btn').addEventListener('click', (e) =>{
   e.preventDefault();
   let selected = document.getElementsByClassName('selected');
   for(let i = 0; i < selected.length; ++i){
       if(selected[i].tagName == 'TH'){
        let obj = new Object;
        obj.action = 'update';
        obj.column = 'ID'
        obj.condition = selected[i].parentElement.SQLid;
        obj.target = selected[i].SQLname;
        obj.prevcond = selected[i].textContent;
        objectConditionHolder = obj;
        ipcRenderer.send('delete:cell', obj);
       }
   }
   table.childNodes.forEach(row =>{
       var counter = 0;
       row.childNodes.forEach(cell =>{
            if(cell.textContent == '')
                ++counter;
       });
       if(counter == row.childNodes.length)
            ipcRenderer.send('delete:row', { id : row.SQLid });
   })
});

document.getElementById('edit_btn').addEventListener('click', (e)=>{
    e.preventDefault();
    let selected = [...document.getElementsByClassName('selected')];
    if(selected.length > 1){
        selected.forEach(element =>{
            element.className = "";
            element.style.background = "#fff"
        });
        return;
    }
    let obj = new Object;
    obj.id = selected[0].parentElement.SQLid;
    obj.target = selected[0].SQLname;
    obj.prevcond = selected[0].textContent;
    console.log(obj);
    objectConditionHolder = obj;    
    ipcRenderer.send('edit:cell', obj);
});

document.getElementById('search_btn').addEventListener('click', (e)=>{
    e.preventDefault();
    let keys = new Array;
    let counter = 0;
    document.getElementById('column_name').childNodes.forEach(cell => {
        keys[counter] = cell.textContent;
        ++counter
    });
    console.log(keys);
    ipcRenderer.send('search:db', keys);
});

const table_events = () => {table.childNodes.forEach(row => {
    row.childNodes.forEach(cell => {
        cell.addEventListener('click', (e) =>{
            e.preventDefault();
            if(cell.className == 'selected'){
                cell.className = "";
                cell.style.background = "#fff";
                return;
            }
            if(cell.parentElement.id != 'column_name'){
                cell.className = 'selected';
                cell.style.background = '#f2db71'
                return;
            }
            
        }, false);
    });
    row.addEventListener('click', (e)=>{
        e.preventDefault();
    });
})};

const showbuttons = () =>{
    document.getElementById('buttonbox').style.display = 'inline-block';
}

ipcRenderer.on('load:db', (event, data)=>{
    hideinput();
    if(data.length > 0){
        console.log(data);
        let keys = Object.keys(data[0]);
        let namerow = document.createElement('tr');
        namerow.id = 'column_name';
        namerow.SQLid = 0;
        for(let i = 1; i<keys.length; ++i){
            let dataholder = document.createElement('th');
            let textNode = document.createTextNode(keys[i]);
            dataholder.appendChild(textNode);
            namerow.appendChild(dataholder);
        }
        table.appendChild(namerow);
        for(let i = 0; i< data.length; ++i){
            let values = Object.values(data[i]);
            let row = document.createElement('tr');
            row.SQLid = values[0];
            for(let j = 1; j < values.length; ++j){
                let dataholder = document.createElement('th');
                dataholder.SQLname = keys[j];
                let textNode = document.createTextNode(values[j]);
                dataholder.appendChild(textNode);
                row.appendChild(dataholder);
            }
            table.appendChild(row);
        }
        content[0].insertBefore(table, buttonbox);
        table_events();
    }
        showbuttons();

}); 

ipcRenderer.on('succsess_add:row', (e, obj)=>{
    let row = document.createElement('tr');
    row.SQLid = obj.id;
    delete obj.id;
    let values = Object.values(obj);
    let keys = Object.keys(obj);
    for(let j = 0; j < values.length; ++j){
        let dataholder = document.createElement('th');
        let textNode = document.createTextNode(values[j]);
        dataholder.appendChild(textNode);
        dataholder.SQLname = keys[j];
        row.appendChild(dataholder);
    }
    row.childNodes.forEach(cell => {
        cell.addEventListener('click', (e) =>{
            e.preventDefault();
            if(cell.className == 'selected'){
                cell.className = "";
                cell.style.background = "#fff";
                return;
            }
            if(cell.parentElement.id != 'column_name'){
                cell.className = 'selected';
                cell.style.background = '#f2db71'
                return;
            }
            
        }, false);
    });
    row.addEventListener('click', (e)=>{
        e.preventDefault();
    });
    table.appendChild(row);
});

ipcRenderer.on('succsess_delete:cell', (e, obj) =>{
    table.childNodes.forEach(row =>{
        if(row.SQLid == obj.condition){
            row.childNodes.forEach(cell =>{
                if(cell.textContent == obj.prevcond){
                    cell.textContent = "";
                    cell.className = "";
                    cell.style.background = "#fff"
                }
            })
        }
    });
})

ipcRenderer.on('succsess_delete:row', (e, obj) =>{
    table.childNodes.forEach(row =>{
        if(row.SQLid == obj.id){
            row.remove();
        }
    });
});

ipcRenderer.on('edit_data:cell', (e, obj)=>{
    var flag = false;
    table.childNodes.forEach(row =>{
        if(row.SQLid == obj.id){
            row.childNodes.forEach(cell =>{
                if(!flag && cell.SQLname == obj.target && cell.textContent == obj.prevcond){
                    cell.textContent = obj.newcond;
                    cell.className = "";
                    cell.style.background = "#fff"
                    flag = true;
                }
            });
        }
    });
});

ipcRenderer.on('open:db', (e)=>{
    input.click();
});

ipcRenderer.on('search_data:db', (e, data)=>{
    table.innerHTML = "";
    console.log(data);
    let keys = Object.keys(data[0]);
    let namerow = document.createElement('tr');
    namerow.id = 'column_name';
    namerow.SQLid = 0;
    for(let i = 1; i<keys.length; ++i){
        let dataholder = document.createElement('th');
        let textNode = document.createTextNode(keys[i]);
        dataholder.appendChild(textNode);
        namerow.appendChild(dataholder);
    }
    table.appendChild(namerow);
    for(let i = 0; i< data.length; ++i){
        let values = Object.values(data[i]);
        let row = document.createElement('tr');
        row.SQLid = values[0];
        for(let j = 1; j < values.length; ++j){
            let dataholder = document.createElement('th');
            dataholder.SQLname = keys[j];
            let textNode = document.createTextNode(values[j]);
            dataholder.appendChild(textNode);
            row.appendChild(dataholder);
        }
        table.appendChild(row);
    }
    content[0].insertBefore(table, buttonbox);
    table_events();

});